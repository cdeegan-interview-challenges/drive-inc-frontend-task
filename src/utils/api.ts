export const createReservationRequest = async (location, vehicle, startDate, duration) => {
  try {
    const response = await fetch(`${import.meta.env.VITE_BASE_API_URL}/reserve/request`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        location: location,
        vehicleType: vehicle,
        startDateTime: startDate.toISOString(),
        duration: duration
      })
    })

    const responseBody = await response.json()
    if (response.status !== 200) {
      return {
        success: false,
        data: JSON.stringify(responseBody.errors)
      }
    } else if (responseBody.success === false) {
      return {
        success: false,
        data: responseBody.message
      }
    } else {
      return {
        success: true,
        data: responseBody
      }
    }
  } catch (e) {
    return {
      success: false,
      data: 'An error occurred, please try again later'
    }
  }
}

export const createReservationConfirmation = async (vehicleId, startDate, duration, cName, cEmail, cPhone) => {
  try {
    const response = await fetch(`${import.meta.env.VITE_BASE_API_URL}/reserve/confirm`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        vehicleId: vehicleId,
        startDateTime: startDate.toISOString(),
        durationMins: duration,
        customerName: cName,
        customerPhone: cPhone,
        customerEmail: cEmail
      })
    })

    const responseBody = await response.json()
    if (response.status !== 200) {
      return {
        success: false,
        data: JSON.stringify(responseBody.errors)
      }
    } else if (responseBody.success === false) {
      return {
        success: false,
        data: responseBody.message
      }
    } else {
      return {
        success: true,
        data: responseBody
      }
    }
  } catch (e) {
    return {
      success: false,
      data: 'An error occurred, please try again later'
    }
  }
}

